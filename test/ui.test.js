const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true;

// // 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// // 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    await browser.close()
})

// // 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({ path: 'test/screenshots/login.png' });
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitForSelector('#users > ol > li');

    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/welcome.png', delay: 1000 });
    await browser.close()
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', '', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', '', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });


    // await page.waitForSelector('#users > ol > li');
    // let member = await page.evaluate(() => {
    //     return document.querySelector('#users > ol > li').innerHTML;
    // });

    // expect(member).toBe('John');
    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/error.png', delay: 1000 });

    await browser.close()

})

// 6
test('[Content] Welcome message', async () => {
    let userName = "Tim";
    let roomName = "R1";
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', userName, { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });


    await page.waitFor(2000)
    let welcomeMessage = await page.$eval('#messages > li:nth-child(1) > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(welcomeMessage).toBe("Hi " + userName + ", Welcome to the chat app");
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// // 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitForSelector('#users > ol > li');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');

    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage()
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage()

    await page1.goto(url)
    await page2.goto(url)

    let user1Name = "Tim";
    let user2Name = "Mark";
    let roomName = "Room1";
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', user1Name, { delay: 100 });
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, { delay: 100 });
    await page1.keyboard.press('Enter', { delay: 100 });

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', user2Name, { delay: 100 });
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, { delay: 100 });
    await page2.keyboard.press('Enter', { delay: 100 });

    await page1.waitFor(2000);
    await page2.waitFor(2000);

    let member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    let member2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    console.log(member1)
    console.log(member2)
    if(member1 == "Tim" && member2 == "Mark") {
        expect("1").toBe("1");
    }
    else 
        expect("1").toBe("0");

    await browser1.close();
    await browser2.close();

})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    // let sendButton = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    let sendButton = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    console.log(sendButton)
    expect(sendButton).toBe('Send');

    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    // user login
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitFor(3000);

    await page.type('#message-form > input[type=text]', 'Hello World');

    let message = await page.evaluate(() => {
        return document.querySelector('#message-form > input[type=text]').value;
    });
    console.log(message)
    expect(message).toBe("Hello World");

    await page.focus("#message-form > button")
    await page.keyboard.press('Enter', { delay: 100 });
    // await page.click('#message-form > button');
    await page.waitFor(1000);

    await browser.close();
})

// // 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const message1 = "HI";
    const message2 = "Hello";
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage()
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage()

    await page1.goto(url)
    await page2.goto(url)

    let user1Name = "John";
    let user2Name = "Mike";
    let roomName = "Room1";
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', user1Name, { delay: 100 });
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, { delay: 100 });
    await page1.keyboard.press('Enter', { delay: 100 });

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', user2Name, { delay: 100 });
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, { delay: 100 });
    await page2.keyboard.press('Enter', { delay: 100 });

    // wait for welcome message
    await page1.waitFor(2000);
    await page2.waitFor(2000)


    // Send Message to eachother
    await page1.type('#message-form > input[type=text]', message1);
    await page1.focus("#message-form > button")
    await page1.keyboard.press('Enter', { delay: 100 });
    await page1.waitFor(500);

    await page2.type('#message-form > input[type=text]', message2);
    await page2.focus("#message-form > button")
    await page2.keyboard.press('Enter', { delay: 100 });
    await page2.waitFor(500);

    let JohnMessage = await page2.$eval('#messages > li:nth-child(2) > div:nth-child(2) > p', (content) => content.innerHTML);
    let MikeMessage = await page2.$eval('#messages > li:nth-child(3) > div:nth-child(2) > p', (content) => content.innerHTML);
    if(JohnMessage == message1 && MikeMessage == message2) {
        expect("1").toBe("1");
    }
    else
        expect("1").toBe("2");

    await browser1.close();
    await browser2.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    // let sendButton = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    let sendButton = await page.evaluate(() => {
        return document.querySelector('.chat__footer > button').innerHTML;
    });
    console.log(sendButton)
    expect(sendButton).toBe('Send location');

    await browser.close();
})

// // 15
test('[Behavior] Send a location message', async () => {
    // user login
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitFor(3000);

    await page.focus("#send-location");
    await page.keyboard.press('Enter', { delay: 100 });
    await browser.close();
})
